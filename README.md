# STRANGER THINGS CODE CHALLENGE

## 1. DESCRIPTION

This live coding challenge will have two tasks. If the candidate only gets through the first task, that is OK. They will need to write a Node.js script to fetch and process a JSON data set, extracting information from it and performing a transformation on the data. The script should console.log their answers.

## 2. JSON

[Stranger Things JSON](http://api.tvmaze.com/singlesearch/shows?q=stranger-things&embed=episodes)

## 3. RULES

- Timebox the candidate to around 45 minutes for the technical portion. Leave the remaining 15 minutes for them to ask questions about the role or the team.

- This is “open Google.” Let them look up any documentation to their heart’s content.

- Let them know they can ask as many questions as they need. Don’t be afraid to clarify things for them. Sometimes instructions can be confusing.

## 4. TASKS

- Using the JSON data of the Stranger Things TV show, write code to discover the 5 most commonly used words in the “summary” property of the “episodes” objects. Console log those 5 most popular summary words along with how many instances of the word were found. The calculation for this metric should not be case sensitive and should ignore periods, commas, colons, quotation marks, or semi-colons. The logic should, however, take apostrophes into account. (Example, “the (50), boy’s (37), Will (30), mysterious (25), her (15)”)

- Re-format the json into the following structure using existing data. Console log the full result.
  ```
  {
      <showId>: {
          totalDurationSec: ... // Total duration of the show, across all episodes (seconds)
          averageEpisodesPerSeason: ... // Average episodes per season, float with max one decimal (e.g. 5.3)
          episodes: {
              <episodeId>: {
                  sequenceNumber: s<X>e<Y> // Episode and season number, e.g. "s1e1"
                  shortTitle: ... // Title without "Chapter XXX:" prefix
                  airTimestamp: ... // Air timestamp in epoch time (seconds)
                  shortSummary: ... // First sentence of the summary, without HTML tags
              }
              ...
          }
      }
  }
  ```
